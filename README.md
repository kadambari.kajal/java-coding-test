# Checkout project

git clone git@gitlab.com:kadambari.kajal/java-coding-test.git

# Build Steps (using Maven)

cd java-coding-test\
mvn clean install

# Run
cd target\
java -jar java-coding-test.war

# Test App(Entry URL)

http://localhost:8080/java-coding-test/welcome 
