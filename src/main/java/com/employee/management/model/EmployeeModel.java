package com.employee.management.model;

import lombok.Data;

@Data
public class EmployeeModel {

	private Long employeeId;

	private String firstname;

	private String lastname;

	private String dob;

	private Long departmentId;

	private String departmentName;

	private String salary;

	private Long managerId;

	private String managerName;
}
