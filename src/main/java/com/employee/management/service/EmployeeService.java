package com.employee.management.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.employee.management.exception.InternalServerException;
import com.employee.management.exception.UserNotFoundException;
import com.employee.management.model.EmployeeModel;
import com.employee.management.repositories.DepartmentRepository;
import com.employee.management.repositories.EmployeeRepository;
import com.employee.management.utils.DateUtils;
import com.employee.management.vo.Department;
import com.employee.management.vo.Employee;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	DateUtils dateUtils;

	@Transactional(rollbackFor = { Exception.class })
	public EmployeeModel createEmployee(EmployeeModel employeeModel) {

		EmployeeModel employeeToEmployeeModel = null;
		try {
			Employee employee = new Employee();
			employee.setFirstname(employeeModel.getFirstname());
			employee.setLastname(employeeModel.getLastname());
			employee.setDob(dateUtils.stringToLocalDate(employeeModel.getDob()));

			if (!ObjectUtils.isEmpty(employeeModel.getManagerId())) {
				long parseLong = employeeModel.getManagerId();
				employee.setManager(parseLong);
			}

			Department findByDepartmentName = departmentRepository.findById(employeeModel.getDepartmentId()).get();
			employee.setDepartment(findByDepartmentName);
			if (ObjectUtils.isEmpty(findByDepartmentName)) {
				findByDepartmentName.getEmployees().add(employee);
			} else {
				findByDepartmentName.setEmployees(Collections.singletonList(employee));
			}

			double salary = Double.parseDouble(employeeModel.getSalary());
			employee.setSalary(salary);

			employee = employeeRepository.save(employee);
			employeeToEmployeeModel = employeeToEmployeeModel(employee);
		} catch (NumberFormatException e) {
			log.error("Error while creating Employee record");
			throw new InternalServerException("Error while creating Employee record");
		} catch (Exception e) {
			log.error("Error while creating Employee record");
			throw new InternalServerException("Error while creating Employee record");
		}
		return employeeToEmployeeModel;
	}

	public List<EmployeeModel> getEmployee() {
		List<Employee> findAll = employeeRepository.findAll();
		List<EmployeeModel> collect = findAll.stream().map(e -> employeeToEmployeeModel(e))
				.collect(Collectors.toList());
		return collect;
	}

	public EmployeeModel getEmployee(long employeeId) {
		Optional<Employee> employeeOptional = employeeRepository.findById(employeeId);
		if (!employeeOptional.isPresent()) {
			String format = String.format("Could not find Employee with id %s", employeeId);
			log.error(format);
			throw new UserNotFoundException(format);
		}
		EmployeeModel employeeModel = employeeToEmployeeModel(employeeOptional.get());
		return employeeModel;
	}

	public List<EmployeeModel> getManagers() {
		List<Employee> findAll = employeeRepository.findAll();
		List<EmployeeModel> collect = findAll.stream().map(employee -> {
			EmployeeModel employeeModel = new EmployeeModel();
			employeeModel.setManagerName(employee.getFirstname() + " " + employee.getLastname());
			employeeModel.setManagerId(employee.getId());
			return employeeModel;
		}).collect(Collectors.toList());
		return collect;
	}

	public EmployeeModel employeeToEmployeeModel(Employee employee) {
		EmployeeModel employeeModel = new EmployeeModel();

		employeeModel.setEmployeeId(employee.getId());
		employeeModel.setFirstname(employee.getFirstname());
		employeeModel.setLastname(employee.getLastname());
		employeeModel.setDob(dateUtils.localDateToString(employee.getDob()));
		employeeModel.setSalary(String.valueOf(employee.getSalary()));
		employeeModel.setDepartmentName(employee.getDepartment().getDepartmentName());
		if (!ObjectUtils.isEmpty(employee.getManager())) {
			Optional<Employee> employeeManagerOptional = employeeRepository.findById(employee.getManager());
			employeeManagerOptional.ifPresent(manager -> {
				employeeModel.setManagerName(manager.getFirstname() + " " + manager.getLastname());
				employeeModel.setManagerId(manager.getId());
			});
		}
		return employeeModel;
	}

	public static EmployeeModel updateEmployee(EmployeeModel employee) {
		return null;

	}
}
