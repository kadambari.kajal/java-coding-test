<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
 
</head>
<body>
	<div class="container">
		<h2>Employee Details</h2>
		</br>
		<button type="button" class="btn btn-primary  pull-rigth">Edit</button>
		</br></br>
		<form action="${pageContext.request.contextPath}/saveEmployee" method="POST">
		<input type="hidden" class="form-control" id="employeeId" name="employeeId" required>
			<div class="form-group">
				<label for="firstName">First Name:</label> 
				<input type="text" class="form-control" id="firstname" name="firstname" readonly="readonly" value="${employee.firstname}"></input>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name:</label> <input type="text"
					class="form-control" id="lastname" name="lastname" readonly="readonly" value="${employee.lastname}">
			</div>
			<div class="form-group">
				<label for="dob">DOB:</label> <input type="text"
					class="form-control" id="dob" name="dob" readonly="readonly" value="${employee.dob}">
			</div>
			<div class="form-group">
				<label for="department">Department:</label>	
				<input type="text" class="form-control" id="departmentName" name="departmentName" readonly="readonly" value="${employee.departmentName}">
			</div>
			<div class="form-group">
				<label for="manager">Manager:</label>
				<input type="text" class="form-control" id="managerName" name="managerName" readonly="readonly" value="${employee.managerName}">
			</div>
			<div class="form-group">
				<label for="salary">Salary:</label> <input type="text"
					class="form-control" id="salary" name="salary" readonly="readonly" value="${employee.salary}">
			</div>
		</form>
	</div>
</body>
</html>