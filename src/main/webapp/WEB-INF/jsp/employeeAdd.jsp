<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html lang="en">
<head>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){

 
	
	$("#employeeForm").submit(function (evt) {
	
		  //At this point the browser will have performed default validation methods.
		  
		  //If you want to perform custom validation do it here.
		  if ($("input[Name='firstname']").val().length < 0) {
		    alert("First Name is requried");
		    evt.preventDefault();
		    return;
		  }else if ($("input[Name='lastname']").val().length < 0) {
			    alert("Last Name is requried");
			    evt.preventDefault();
			    return;
		  }else if ($("input[Name='dob']").val().length < 0) {
			    alert("Date of birth is requried");
			    evt.preventDefault();
			    return;
		  }else if ($("input[Name='salary']").val().length < 0) {
			    alert("Salary is requried");
			    evt.preventDefault();
			    return;
		  }else if ($("input[Name='departmentId']").val() == '') {
			    alert("Department is requried");
			    evt.preventDefault();
			    return;
		  }
		 //alert("Values are correct!");

		});
	
});
</script>

</head>
<body>
	<div class="container">
		<h2>Employee Form</h2>
		<form action="${pageContext.request.contextPath}/employee/save" method="POST" name="employeeForm" id="employeeForm">
			<div class="form-group">
				<label for="firstName">First Name:</label> <input type="text"
					class="form-control" id="firstname" name="firstname" required>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name:</label> <input type="text"
					class="form-control" id="lastname" name="lastname" required>
			</div>
			<div class="form-group">
				<label for="dob">DOB:</label> <input type="text"
					class="form-control" id="dob" name="dob" required placeholder="DD/MM/YYYY">
			</div>
			<div class="form-group">
				<label for="department">Department:</label> 
				<select class="form-control" id="departmentId" name="departmentId">
					<c:forEach var="department" items="${departments}">
						<option value="${department.departmentId}">${department.departmentName}</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="manager">Manager:</label> 
				<select class="form-control" id="managerId" name="managerId">
					<option value="">No Manager Assigned</option>
					<c:forEach var="manager" items="${managers}">
						<option value="${manager.managerId}">${manager.managerName}</option>
					</c:forEach> 
				</select>
			</div>
			<div class="form-group">
				<label for="salary">Salary:</label> 
				<input type="number" class="form-control" id="salary" name="salary" required>
			</div>
			<button type="submit" class="btn btn-primary">Create</button>
		</form>
	</div>

</body>

</html>